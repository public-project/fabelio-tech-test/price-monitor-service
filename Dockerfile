FROM openjdk:8-jre-alpine
COPY target/price-monitor-service-0.0.1-SNAPSHOT.jar /app/price-monitor-service.jar
ENTRYPOINT ["java", "-jar", "/app/price-monitor-service.jar"]