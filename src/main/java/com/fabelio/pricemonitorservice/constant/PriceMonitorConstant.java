package com.fabelio.pricemonitorservice.constant;

public class PriceMonitorConstant {

	public static final String HTML_ATTR_TYPE = "type";
	public static final String HTML_TAG_SCRIPT = "script";
	public static final String APPLICATION_LD_JSON = "application/ld+json";
	
}
