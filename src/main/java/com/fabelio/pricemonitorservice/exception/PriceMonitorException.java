package com.fabelio.pricemonitorservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class PriceMonitorException extends RuntimeException {

	private static final long serialVersionUID = 6571073751563813054L;

	public PriceMonitorException() {
		super();
	}

	public PriceMonitorException(String message, Throwable cause) {
		super(message, cause);
	}

	public PriceMonitorException(String message) {
		super(message);
	}

	public PriceMonitorException(Throwable cause) {
		super(cause);
	}

}
