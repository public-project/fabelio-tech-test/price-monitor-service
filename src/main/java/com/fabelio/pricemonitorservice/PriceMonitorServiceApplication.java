package com.fabelio.pricemonitorservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PriceMonitorServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PriceMonitorServiceApplication.class, args);
	}

}
