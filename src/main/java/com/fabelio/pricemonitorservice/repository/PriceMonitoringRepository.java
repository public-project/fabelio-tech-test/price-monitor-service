package com.fabelio.pricemonitorservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.fabelio.pricemonitorservice.object.Product;

@Repository
public interface PriceMonitoringRepository extends CrudRepository<Product, String> {

}
