package com.fabelio.pricemonitorservice.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DummyController {

	private static final Logger log = LoggerFactory.getLogger(DummyController.class);

	@PostMapping(path = "echo")
	public String echo(@RequestBody String requestBody) throws IOException {
		log.info("body {}", requestBody);
		return "echo world";
	}

}
