package com.fabelio.pricemonitorservice.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fabelio.pricemonitorservice.object.Product;
import com.fabelio.pricemonitorservice.service.PriceMonitorService;

@CrossOrigin(origins = "http://localhost:8080/")
@RestController
@RequestMapping(path = "api")
public class PriceMonitorController {

	private static final Logger log = LoggerFactory.getLogger(PriceMonitorController.class);

	@Autowired
	private PriceMonitorService priceMonitorService;

	@PostMapping(path = "product/fetch", consumes = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<Product> getProductInformation(@RequestBody String body) {
		log.info("start fetch product");
		Product responseBody = priceMonitorService.extract(body);
		return new ResponseEntity<>(responseBody, HttpStatus.OK);
	}

	@GetMapping(path = "product/{sku}")
	public ResponseEntity<Product> getProduct(@PathVariable(name = "sku") String sku) {
		log.info("get product");
		Product product = priceMonitorService.fetchProduct(sku);
		return new ResponseEntity<>(product, HttpStatus.OK);
	}

	@GetMapping(path = "product/list")
	public ResponseEntity<List<Product>> getProducts() {
		log.info("get all product");
		List<Product> products = priceMonitorService.getProducts();
		return new ResponseEntity<>(products, HttpStatus.OK);
	}

}
