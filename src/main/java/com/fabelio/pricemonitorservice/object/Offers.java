package com.fabelio.pricemonitorservice.object;

import java.util.Objects;

public class Offers {

	private String price;
	private String priceCurrency;

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getPriceCurrency() {
		return priceCurrency;
	}

	public void setPriceCurrency(String priceCurrency) {
		this.priceCurrency = priceCurrency;
	}

	@Override
	public int hashCode() {
		return Objects.hash(price, priceCurrency);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Offers)) {
			return false;
		}
		Offers other = (Offers) obj;
		return Objects.equals(price, other.price) && Objects.equals(priceCurrency, other.priceCurrency);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Offers [price=").append(price).append(", priceCurrency=").append(priceCurrency).append("]");
		return builder.toString();
	}

}
