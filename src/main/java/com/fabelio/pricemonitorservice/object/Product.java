package com.fabelio.pricemonitorservice.object;

import java.util.Objects;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@RedisHash("Product")
public class Product {

	@Id
	private String sku;
	private String name;
	private String description;
	private Offers offers;

	public Product() {
		super();
	}

	public Product(String sku, String name, String description, Offers offers) {
		this.sku = sku;
		this.name = name;
		this.description = description;
		this.offers = offers;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Offers getOffers() {
		return offers;
	}

	public void setOffers(Offers offers) {
		this.offers = offers;
	}

	@Override
	public int hashCode() {
		return Objects.hash(description, name, offers, sku);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Product)) {
			return false;
		}
		Product other = (Product) obj;
		return Objects.equals(description, other.description) && Objects.equals(name, other.name)
				&& Objects.equals(offers, other.offers) && Objects.equals(sku, other.sku);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductObject [sku=").append(sku).append(", name=").append(name).append(", description=")
				.append(description).append(", offers=").append(offers).append("]");
		return builder.toString();
	}

}
