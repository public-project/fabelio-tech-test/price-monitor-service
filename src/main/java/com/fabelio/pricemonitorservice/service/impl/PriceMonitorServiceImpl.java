package com.fabelio.pricemonitorservice.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fabelio.pricemonitorservice.constant.PriceMonitorConstant;
import com.fabelio.pricemonitorservice.exception.PriceMonitorException;
import com.fabelio.pricemonitorservice.object.Product;
import com.fabelio.pricemonitorservice.repository.PriceMonitoringRepository;
import com.fabelio.pricemonitorservice.service.PriceMonitorService;
import com.google.gson.Gson;

@Service
public class PriceMonitorServiceImpl implements PriceMonitorService {

	private static final Logger log = LoggerFactory.getLogger(PriceMonitorServiceImpl.class);
	
	@Autowired
	private PriceMonitoringRepository priceMonitoringRepository;

	@Override
	public Product extract(String body) {
		log.info("extracting HTML page {}", body);
		try {
			Document html = Jsoup.connect(body).get();
			Elements select = html.getElementsByTag(PriceMonitorConstant.HTML_TAG_SCRIPT);
			Gson gson = new Gson();
			Product productObject = null;

			for (Element element : select) {
				String attr = element.attr(PriceMonitorConstant.HTML_ATTR_TYPE);
				if (attr.contentEquals(PriceMonitorConstant.APPLICATION_LD_JSON)) {
					productObject = gson.fromJson(element.data(), Product.class);
					log.info("find product information and saving...");
					priceMonitoringRepository.save(productObject);
				}
			}
			return productObject;
		} catch (IOException e) {
			log.error("Error on extracting HTML with url {}", body);
			throw new PriceMonitorException("Error extracting body");
		}

	}
	
	@Override
	public Product fetchProduct(String sku) {
		log.info("find product by sku {}", sku);
		return priceMonitoringRepository.findById(sku).orElse(null);
	}
	
	@Override
	public List<Product> getProducts() {
		log.info("find all product");
		List<Product> listProduct = new ArrayList<>();
		priceMonitoringRepository.findAll().forEach(listProduct::add);
		return listProduct;
	}

}
