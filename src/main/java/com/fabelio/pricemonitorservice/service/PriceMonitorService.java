package com.fabelio.pricemonitorservice.service;

import java.util.List;

import com.fabelio.pricemonitorservice.object.Product;

public interface PriceMonitorService {

	Product extract(String body);
	
	Product fetchProduct(String sku);
	
	List<Product> getProducts();

}
