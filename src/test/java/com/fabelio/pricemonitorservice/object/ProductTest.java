package com.fabelio.pricemonitorservice.object;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class ProductTest {
	
	@Test
	public void testProduct() {
		Offers offers = new Offers();
		offers.setPrice("1000");
		offers.setPriceCurrency("IDR");
		
		Product product = new Product();
		product.setName("DUMMY_NAME");
		product.setDescription("DUMMY_DESC");
		product.setSku("DUMMY_SKU");
		product.setOffers(offers);
		
		assertEquals("DUMMY_NAME", product.getName());
		assertEquals("DUMMY_DESC", product.getDescription());
		assertEquals("DUMMY_SKU", product.getSku());
		assertEquals(offers, product.getOffers());
	}
	
	@Test
	public void testProductConstructor() {
		Offers offers = new Offers();
		offers.setPrice("1000");
		offers.setPriceCurrency("IDR");
		
		Product product = new Product("DUMMY_NAME", "DUMMY_DESC", "DUMMY_SKU", offers);
		
		assertNotNull(product.getOffers());
	}
	
}
