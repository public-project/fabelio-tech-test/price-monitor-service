package com.fabelio.pricemonitorservice.object;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class OffersTest {

	@Test
	public void testOffers() {
		Offers offers = new Offers();
		offers.setPrice("39999");
		offers.setPriceCurrency("IDR");
		
		assertEquals("39999", offers.getPrice());
		assertEquals("IDR", offers.getPriceCurrency());
	}

}
