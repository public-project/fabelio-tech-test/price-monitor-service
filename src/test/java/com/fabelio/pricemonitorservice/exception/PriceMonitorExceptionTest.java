package com.fabelio.pricemonitorservice.exception;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class PriceMonitorExceptionTest {

	@Test
	public void testPriceMonitorExceptionWithMessageAndThrowable() {
		String message = "MESSAGE";
		Throwable throwable = new Throwable();
		
		PriceMonitorException exception = new PriceMonitorException(message, throwable);
		
		assertEquals(message, exception.getMessage());
	}
	
	@Test
	public void testPriceMonitorExceptionWithMessage() {
		String message = "MESSAGE";
		
		PriceMonitorException exception = new PriceMonitorException(message);
		
		assertEquals(message, exception.getMessage());
	}
	
	@Test
	public void testPriceMonitorExceptionWithThrowable() {
		Throwable throwable = new Throwable();
		
		PriceMonitorException exception = new PriceMonitorException(throwable);
		
		assertEquals(throwable, exception.getCause());
	}
	
	@Test
	public void testDefaultConstructor() {
		PriceMonitorException exception = new PriceMonitorException();
		assertNotNull(exception);
	}

}
