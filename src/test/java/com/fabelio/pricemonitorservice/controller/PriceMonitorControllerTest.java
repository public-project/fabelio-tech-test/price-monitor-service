package com.fabelio.pricemonitorservice.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import com.fabelio.pricemonitorservice.object.Offers;
import com.fabelio.pricemonitorservice.object.Product;
import com.fabelio.pricemonitorservice.service.PriceMonitorService;

public class PriceMonitorControllerTest {
	
	@InjectMocks
	private PriceMonitorController controller;
	
	@Mock
	private PriceMonitorService service;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetProductInformation() {
		String body = "http://dummy.com";
		Offers offers = new Offers();
		offers.setPrice("1000");
		offers.setPriceCurrency("IDR");
		
		Product product = new Product();
		product.setName("DUMMY_NAME");
		product.setDescription("DUMMY_DESC");
		product.setSku("DUMMY_SKU");
		product.setOffers(offers);
		
		Mockito.when(service.extract(body)).thenReturn(product);
		ResponseEntity<Product> actual = controller.getProductInformation(body);
		
		assertEquals(product, actual.getBody());
		
		Mockito.verify(service, times(1)).extract(body);
		Mockito.verifyNoMoreInteractions(service);
	}
	
	@Test
	public void testGetProduct() {
		String sku = "DUMMY_SKU";
		Offers offers = new Offers();
		offers.setPrice("1000");
		offers.setPriceCurrency("IDR");
		
		Product product = new Product();
		product.setName("DUMMY_NAME");
		product.setDescription("DUMMY_DESC");
		product.setSku("DUMMY_SKU");
		product.setOffers(offers);
		
		Mockito.when(service.fetchProduct(sku)).thenReturn(product);
		ResponseEntity<Product> actual = controller.getProduct(sku);
		
		assertEquals(product, actual.getBody());
		
		Mockito.verify(service, times(1)).fetchProduct(sku);
		Mockito.verifyNoMoreInteractions(service);
	}
	
	@Test
	public void testProducts() {
		Offers offers = new Offers();
		offers.setPrice("1000");
		offers.setPriceCurrency("IDR");
		
		Product product = new Product();
		product.setName("DUMMY_NAME");
		product.setDescription("DUMMY_DESC");
		product.setSku("DUMMY_SKU");
		product.setOffers(offers);
		
		List<Product> listProducts = new ArrayList<>();
		listProducts.add(product);
		
		Mockito.when(service.getProducts()).thenReturn(listProducts);
		ResponseEntity<List<Product>> actual = controller.getProducts();
		
		assertEquals(listProducts, actual.getBody());
		
		Mockito.verify(service, times(1)).getProducts();
		Mockito.verifyNoMoreInteractions(service);
	}

}
