package com.fabelio.pricemonitorservice.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.fabelio.pricemonitorservice.exception.PriceMonitorException;
import com.fabelio.pricemonitorservice.object.Offers;
import com.fabelio.pricemonitorservice.object.Product;
import com.fabelio.pricemonitorservice.repository.PriceMonitoringRepository;
import com.fabelio.pricemonitorservice.service.impl.PriceMonitorServiceImpl;

public class PriceMonitorServiceImplTest {

	@InjectMocks
	private PriceMonitorServiceImpl service;

	@Mock
	private PriceMonitoringRepository repository;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testExtractWhenSuccess() {
		String body = "https://fabelio.com/ip/kursi-kantor-alpha.html";
		String sku = "F61FD03-000V00ALP";
		
		Offers offers = new Offers();
		offers.setPrice("629100");
		offers.setPriceCurrency("IDR");
		
		Product product = new Product();
		product.setSku(sku);
		product.setName("Kursi Kantor Alpha");
		product.setDescription("<p><br /><em>NB:&nbsp;Untuk produk yang memerlukan perakitan, akan dilakukan beberapa hari setelah produk dikirim. Apabila telah melebih dari 7 (tujuh) hari setelah produk dikirim belum ada tim kami yang menghubungi jadwal perakitan, Mohon segera hubungi tim Customer Service kami.</em></p>");
		product.setOffers(offers);
		
		Mockito.when(repository.save(product)).thenReturn(null);
		Product actual = service.extract(body);

		assertEquals(sku, actual.getSku());

		Mockito.verify(repository, times(1)).save(product);
		Mockito.verifyNoMoreInteractions(repository);
	}

	@Test(expected = PriceMonitorException.class)
	public void testExtractWhenReturnException() {
		String body = "http://dummy.html";

		service.extract(body);

		Mockito.verify(repository, times(0)).save(null);
		Mockito.verifyNoMoreInteractions(repository);
	}

	@Test
	public void testFetchProductWhenIdNotNull() {
		String sku = "DUMMY_ID";
		Optional<Product> result = Optional.of(new Product());
		result.get().setSku(sku);
		result.get().setName("DUMMY_NAME");
		result.get().setDescription("DUMMY_DESC");

		Mockito.when(repository.findById(sku)).thenReturn(result);
		Product actual = service.fetchProduct(sku);

		assertEquals(actual.getName(), result.get().getName());

		Mockito.verify(repository, times(1)).findById(sku);
		Mockito.verifyNoMoreInteractions(repository);
	}

	@Test
	public void testFetchProductWhenIdNull() {
		String sku = null;

		Mockito.when(repository.findById(sku)).thenReturn(Optional.of(new Product()));
		Product actual = service.fetchProduct(sku);

		assertNotNull(actual);

		Mockito.verify(repository, times(1)).findById(sku);
		Mockito.verifyNoMoreInteractions(repository);
	}

	@Test
	public void testGetProductsReturnNotNull() {
		String sku = "DUMMY_ID";
		Product product = new Product();
		product.setSku(sku);
		product.setName("DUMMY_NAME");
		product.setDescription("DUMMY_DESC");

		List<Product> result = new ArrayList<>();
		result.add(product);

		Mockito.when(repository.findAll()).thenReturn(result);
		List<Product> actual = service.getProducts();

		assertEquals(1, actual.size());

		Mockito.verify(repository, times(1)).findAll();
		Mockito.verifyNoMoreInteractions(repository);
	}

	@Test
	public void testGetProductsReturnNull() {
		List<Product> result = new ArrayList<>();

		Mockito.when(repository.findAll()).thenReturn(result);
		List<Product> actual = service.getProducts();

		assertNotNull(actual);

		Mockito.verify(repository, times(1)).findAll();
		Mockito.verifyNoMoreInteractions(repository);
	}

}
