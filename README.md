# Overview
Service to monitor price on fabelio.

This service provide 3 endpoints:

* Fetch product information
* Detail production information
* Get list of products that have been stored

# Prerequisite
- Java > 1.8
- Maven 3.x
- Docker

# Build and Run
Run this command on terminal
```
docker-compose up -d
```
It will build and run price-monitor-service and redis server.


# Fetch product information
### URL
```
http://localhost:8990/api/product/fetch
```

### Method
* POST

### Request Header
#### Content-Type
text/plain

### Request Body
Example
```
https://fabelio.com/ip/meja-kerja-smurf-120.html
```
### Response Body
```
{
    "sku": string,
    "name": string,
    "description": string,
    "offers": {
        "price": string,
        "priceCurrency": string
    }
}
```

# Detail production information
### URL
```
http://localhost:8990/api/product/{sku}
```

### Method
* GET

### Response Body
```
{
    "sku": string,
    "name": string,
    "description": string,
    "offers": {
        "price": string,
        "priceCurrency": string
    }
}
```

# Get list of products
### URL
```
http://localhost:8990/api/product/list
```

### Method
* GET

### Response Body
```
[
    {
        "sku": string,
        "name": string,
        "description": string,
        "offers": {
            "price": string,
            "priceCurrency": string
        }
    }
]
```

# Stop Service
Run this command on terminal
```
docker-compose down
```